package kr.edcan.lumihana.hamony.TimeDialog;

import kr.edcan.lumihana.hamony.ArrayData;

/**
 * Created by kimok_000 on 2016-06-30.
 */
public class TimeData extends ArrayData{
    private String timeText;

    public TimeData(String timeText){
        this.timeText = timeText;
    }

    public String getTimeText() {
        return timeText;
    }
}
