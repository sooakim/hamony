package kr.edcan.lumihana.hamony;

/**
 * Created by kimok_000 on 2016-07-04.
 */
public interface ListViewTool {
    void initListView();
    void addListView(ArrayData data);
    void setListView();
}
