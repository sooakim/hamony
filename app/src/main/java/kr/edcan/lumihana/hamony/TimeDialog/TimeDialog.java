package kr.edcan.lumihana.hamony.TimeDialog;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

import kr.edcan.lumihana.hamony.R;

/**
 * Created by kimok_000 on 2016-06-30.
 */
public abstract class TimeDialog extends Dialog implements View.OnClickListener{
    private TimeAdapter adapter;
    private ArrayList<TimeData> arrayList;
    private ListView timeList;
    private TextView selectedTime;

    protected abstract void onApply(String selctedTime);

    public TimeDialog(Context context) {
        super(context);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        WindowManager.LayoutParams lpWindow = new WindowManager.LayoutParams();
        lpWindow.flags = WindowManager.LayoutParams.FLAG_DIM_BEHIND;
        lpWindow.dimAmount = 0.8f;
        getWindow().setAttributes(lpWindow);

        setContentView(R.layout.dialog_time);

        timeList = (ListView) findViewById(R.id.dialog_list_time);
        selectedTime = (TextView) findViewById(R.id.dialog_text_selectedTime);

        arrayList = new ArrayList<>();
        arrayList.add(new TimeData("1분 후"));
        arrayList.add(new TimeData("3분 후"));
        arrayList.add(new TimeData("5분 후"));
        arrayList.add(new TimeData("10분 후"));
        arrayList.add(new TimeData("30분 후"));
        arrayList.add(new TimeData("1시간 후"));

        adapter = new TimeAdapter(getContext(), arrayList);
        timeList.setAdapter(adapter);

        selectedTime.setOnClickListener(this);

        timeList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String time = arrayList.get(position).getTimeText().toString().trim();
                selectedTime.setText(time);
            }
        });
    }

    @Override
    public void onClick(View v) {
        switch(v.getId()){
            case R.id.dialog_text_selectedTime : {
                if(selectedTime.getText().toString().equals("시간을 선택하세요")){
                    Toast.makeText(getContext(), "시간을 선택해주세요", Toast.LENGTH_SHORT).show();
                    return;
                }

                String time = selectedTime.getText().toString().trim();

                onApply(time);
                break;
            }
        }
    }
}
