package kr.edcan.lumihana.hamony.LoginActivity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import kr.edcan.lumihana.hamony.HamonyService;
import kr.edcan.lumihana.hamony.R;
import kr.edcan.lumihana.hamony.RegisterActivity.RegisterActivity;
import kr.edcan.lumihana.hamony.UserData;
import retrofit.Call;
import retrofit.Callback;
import retrofit.GsonConverterFactory;
import retrofit.Response;
import retrofit.Retrofit;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener{
    private EditText login_edit_id, login_edit_pw;
    private Button login_btn_register, login_btn_login;

    private String id,pw;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        login_btn_login = (Button) findViewById(R.id.login_btn_login);
        login_btn_register = (Button) findViewById(R.id.login_btn_register);
        login_edit_id = (EditText) findViewById(R.id.login_edit_id);
        login_edit_pw = (EditText) findViewById(R.id.login_edit_pw);

        login_btn_login.setOnClickListener(this);
        login_btn_register.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.login_btn_login : onLogin(); break;
            case R.id.login_btn_register : onRegister(); break;
        }
    }

    private void onRegister() {
        Intent intent = new Intent(getApplicationContext(), RegisterActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }

    private void onLogin() {
        id = login_edit_id.getText().toString().trim();
        pw = login_edit_pw.getText().toString().trim();

        if(id.equals("")||pw.equals("")){
            Toast.makeText(getApplicationContext(), "흠.. 뭔가 빠지지 않았나요 >_<;;", Toast.LENGTH_SHORT).show();
            return;
        }

        LoginTask loginTask = new LoginTask();
        loginTask.execute();
    }

    private void loginService(){
        Retrofit retrofit;
        HamonyService hamonyService;
        Call<UserData> logIn;

        retrofit = new Retrofit.Builder()
                .baseUrl("http://malang.moe:9000")
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        hamonyService = retrofit.create(HamonyService.class);
        logIn = hamonyService.login(id, pw);
        logIn.enqueue(new Callback<UserData>() {
            @Override
            public void onResponse(Response<UserData> response, Retrofit retrofit) {
                String id = response.body().get_id();
                int code = response.code();

                Toast.makeText(LoginActivity.this, code + " : " + id, Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onFailure(Throwable t) {
                Toast.makeText(LoginActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    class LoginTask extends AsyncTask<Void, Void, Void> {
        private ProgressDialog dialog;


        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            dialog = ProgressDialog.show(LoginActivity.this, "Sign Up", "Waiting for Server...");
        }

        @Override
        protected Void doInBackground(Void... params) {
            loginService();

            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);

            dialog.dismiss();
        }
    }
}
