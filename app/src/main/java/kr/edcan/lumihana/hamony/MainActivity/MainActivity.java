package kr.edcan.lumihana.hamony.MainActivity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.widget.DrawerLayout;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;

import java.util.ArrayList;

import kr.edcan.lumihana.hamony.ArrayData;
import kr.edcan.lumihana.hamony.DebateDialog.DebateDialog;
import kr.edcan.lumihana.hamony.R;
import kr.edcan.lumihana.hamony.SearchActivity.SearchActivity;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {
    private RecyclerView recyclerView;
    private RecyclerAdapter recyclerAdapter;
    private RecyclerViewDecoration recyclerViewDecoration;
    private ArrayList<RecyclerData> arrayList;
    private Toolbar toolbar;
    private ImageView image_search;
    private StaggeredGridLayoutManager layoutManager;
    private FloatingActionButton fab;
    private DrawerLayout drawer;
    private SwipeRefreshLayout swipeRefreshLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        swipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.main_SRL_refresh);
        recyclerView = (RecyclerView) findViewById(R.id.main_recyclerView);
        image_search = (ImageView) findViewById(R.id.main_image_search);
        fab = (FloatingActionButton) findViewById(R.id.main_fab);
        drawer = (DrawerLayout) findViewById(R.id.main_drawer);
        toolbar = (Toolbar) findViewById(R.id.main_toolbar);
        toolbar.setTitle("Hamony");
        setSupportActionBar(toolbar);

        fab.setOnClickListener(this);
        image_search.setOnClickListener(this);

        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        swipeRefreshLayout.setRefreshing(false);
                    }
                }, 2000);
            }
        });

        layoutManager = new StaggeredGridLayoutManager(2, StaggeredGridLayoutManager.VERTICAL);
        recyclerViewDecoration = new RecyclerViewDecoration(16);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.addItemDecoration(recyclerViewDecoration);

        initRecycler();
        arrayList.add(new RecyclerData(null, null, RecyclerData.DATA_STATUS_DEBATE, 5, 23));
        arrayList.add(new RecyclerData("HelloWorld", null, RecyclerData.DATA_STATUS_SYMPATHY, 5, 23));
        arrayList.add(new RecyclerData("Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.", null, RecyclerData.DATA_STATUS_VOTE, 5, 23));
        arrayList.add(new RecyclerData("HelloWorld", "김수한", RecyclerData.DATA_STATUS_VOTE, 5, 23));
        arrayList.add(new RecyclerData(null, null, RecyclerData.DATA_STATUS_DEBATE, 5, 23));
        arrayList.add(new RecyclerData("HelloWorld", null, RecyclerData.DATA_STATUS_SYMPATHY, 5, 23));
        arrayList.add(new RecyclerData("Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.", null, RecyclerData.DATA_STATUS_VOTE, 5, 23));
        arrayList.add(new RecyclerData("HelloWorld", "김수한", RecyclerData.DATA_STATUS_VOTE, 5, 23));
        arrayList.add(new RecyclerData(null, null, RecyclerData.DATA_STATUS_DEBATE, 5, 23));
        arrayList.add(new RecyclerData("HelloWorld", null, RecyclerData.DATA_STATUS_SYMPATHY, 5, 23));
        arrayList.add(new RecyclerData("Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.", null, RecyclerData.DATA_STATUS_VOTE, 5, 23));
        arrayList.add(new RecyclerData("HelloWorld", "김수한", RecyclerData.DATA_STATUS_VOTE, 5, 23));
        arrayList.add(new RecyclerData(null, null, RecyclerData.DATA_STATUS_DEBATE, 5, 23));
        arrayList.add(new RecyclerData("HelloWorld", null, RecyclerData.DATA_STATUS_SYMPATHY, 5, 23));
        arrayList.add(new RecyclerData("Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.", null, RecyclerData.DATA_STATUS_VOTE, 5, 23));
        arrayList.add(new RecyclerData("HelloWorld", "김수한", RecyclerData.DATA_STATUS_VOTE, 5, 23));
        arrayList.add(new RecyclerData(null, null, RecyclerData.DATA_STATUS_DEBATE, 5, 23));
        arrayList.add(new RecyclerData("HelloWorld", null, RecyclerData.DATA_STATUS_SYMPATHY, 5, 23));
        arrayList.add(new RecyclerData("Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.", null, RecyclerData.DATA_STATUS_VOTE, 5, 23));
        arrayList.add(new RecyclerData("HelloWorld", "김수한", RecyclerData.DATA_STATUS_VOTE, 5, 23));
        arrayList.add(new RecyclerData(null, null, RecyclerData.DATA_STATUS_DEBATE, 5, 23));
        arrayList.add(new RecyclerData("HelloWorld", null, RecyclerData.DATA_STATUS_SYMPATHY, 5, 23));
        arrayList.add(new RecyclerData("Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.", null, RecyclerData.DATA_STATUS_VOTE, 5, 23));
        arrayList.add(new RecyclerData("HelloWorld", "김수한", RecyclerData.DATA_STATUS_VOTE, 5, 23));

    }


    private void initRecycler() {
        arrayList = new ArrayList<>();
        setRecycler();
    }

    private void addRecycler(ArrayData data) {
        arrayList.add((RecyclerData) data);
        setRecycler();
    }

    private void setRecycler() {
        recyclerAdapter = new RecyclerAdapter(getApplicationContext(), arrayList, R.layout.gridview_content_main);
        recyclerView.setAdapter(recyclerAdapter);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.main_fab:
                onFab();
                break;
            case R.id.main_image_search:
                onSearch();
                break;
        }
    }

    private void onSearch() {
        Intent intent = new Intent(getApplicationContext(), SearchActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }

    private void onFab() {
        DebateDialog debateDialog = new DebateDialog(MainActivity.this) {
            @Override
            protected void onApply(String title, String content, String name, String selectedTime) {
                RecyclerData data = new RecyclerData(content, name, RecyclerData.DATA_STATUS_DEBATE, RecyclerData.UPLOAD_TIME_JUST_NOW, 1);

                addRecycler(data);
                dismiss();
            }

            @Override
            protected void onCancel() {
                dismiss();
            }
        };
        debateDialog.show();
    }
}
