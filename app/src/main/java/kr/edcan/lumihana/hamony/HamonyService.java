package kr.edcan.lumihana.hamony;

import retrofit.Call;
import retrofit.http.Field;
import retrofit.http.FormUrlEncoded;
import retrofit.http.POST;

/**
 * Created by kimok_000 on 2016-06-30.
 */
public interface HamonyService {
    int LIST_TYPE_LAST = 1;
    int LIST_TYPE_SPOT = 2;
    int LIST_TYPE_PROGRESS = 3;

    int VOTE_TYPE_GOOD = 0;
    int VOTE_TYPE_BAD = 1;


    @POST("/login")
    @FormUrlEncoded
    Call<UserData> login(
            @Field("id") String id, @Field("pw") String password
    );

    @POST("/signup")
    @FormUrlEncoded
    Call<UserData> sign(
            @Field("id") String id, @Field("pw") String pw,
            @Field("email") String email, @Field("name") String name, @Field("sex") String sex
    );

    @POST("bulletinlist")
    @FormUrlEncoded
    Call<String> listOfPost(@Field("please") int type);

    @POST("writebulletin")
    @FormUrlEncoded
    Call<String> writePost(
            @Field("title") String title, @Field("bulletincontent") String bulletincontent,
            @Field("option") boolean isAnonymous, @Field("bulletinselect") int type,
            @Field("tag") String tag, @Field("exittime") String date
    );

    @POST("/writecomment")
    @FormUrlEncoded
    Call<String> writeComment(
            @Field("bulletinid") int id, @Field("name") String name, @Field("commentcontent") String content
    );

    @POST("/seecomment")
    @FormUrlEncoded
    Call<String> seeComment(@Field("bulletinid") int id);

    @POST("/vote")
    @FormUrlEncoded
    Call<String> vote(
            @Field("bulletinid") int id, @Field("name") String name,
            @Field("vote") int type
    );
}
