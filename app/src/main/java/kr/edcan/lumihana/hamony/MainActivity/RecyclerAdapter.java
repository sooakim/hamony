package kr.edcan.lumihana.hamony.MainActivity;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;

import kr.edcan.lumihana.hamony.GBCommentActivity.CommentActivity;
import kr.edcan.lumihana.hamony.R;

/**
 * Created by kimok_000 on 2016-06-21.
 */
public class RecyclerAdapter extends RecyclerView.Adapter<RecyclerAdapter.ViewHolder> {
    private Context context;
    private ArrayList<RecyclerData> arrayList;
    private int itemLayout;

    public RecyclerAdapter(Context context, ArrayList<RecyclerData> arrayList, int itemLayout) {
        this.context = context;
        this.arrayList = arrayList;
        this.itemLayout = itemLayout;
    }

    @Override
    public RecyclerAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.gridview_content_main, null);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(RecyclerAdapter.ViewHolder holder, int position) {
        final RecyclerData data = arrayList.get(position);
        holder.content.setText(data.getContent() + "");
        holder.commentCount.setText(data.getCommentNum() + "");
        holder.userName.setText(data.getUserName() + "");

        if (data.getUploadTime() == RecyclerData.UPLOAD_TIME_JUST_NOW)
            holder.uploadTime.setText("방금");
        else holder.uploadTime.setText(data.getUploadTime() + "분 전");

        if (data.getCurrentStatus() == RecyclerData.DATA_STATUS_NONE)
            holder.dataStatus.setVisibility(View.GONE);
        else if (data.getCurrentStatus() == RecyclerData.DATA_STATUS_DEBATE)
            holder.dataStatus.setText("토론");
        else if (data.getCurrentStatus() == RecyclerData.DATA_STATUS_COMMENT)
            holder.dataStatus.setText("댓글");
        else if (data.getCurrentStatus() == RecyclerData.DATA_STATUS_SYMPATHY)
            holder.dataStatus.setText("공감");
        else if (data.getCurrentStatus() == RecyclerData.DATA_STATUS_VOTE)
            holder.dataStatus.setText("투표");
    }

    @Override
    public int getItemCount() {
        return this.arrayList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private LinearLayout layoutComment;
        private TextView content, commentCount, userName,
                uploadTime, dataStatus;

        public ViewHolder(View itemView) {
            super(itemView);
            layoutComment = (LinearLayout) itemView.findViewById(R.id.main_Linear_comment);
            content = (TextView) itemView.findViewById(R.id.main_text_content);
            commentCount = (TextView) itemView.findViewById(R.id.main_text_commentCount);
            userName = (TextView) itemView.findViewById(R.id.main_text_userName);
            uploadTime = (TextView) itemView.findViewById(R.id.main_text_uploadTime);
            dataStatus = (TextView) itemView.findViewById(R.id.main_text_dataStatus);

            layoutComment.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(context, CommentActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    context.startActivity(intent);
                }
            });
        }
    }
}
