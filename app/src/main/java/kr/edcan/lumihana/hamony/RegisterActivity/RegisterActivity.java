package kr.edcan.lumihana.hamony.RegisterActivity;

import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import kr.edcan.lumihana.hamony.HamonyService;
import kr.edcan.lumihana.hamony.R;
import kr.edcan.lumihana.hamony.UserData;
import retrofit.Call;
import retrofit.Callback;
import retrofit.GsonConverterFactory;
import retrofit.Response;
import retrofit.Retrofit;

public class RegisterActivity extends AppCompatActivity implements View.OnClickListener {
    private EditText register_edit_id, register_edit_pw, register_edit_pwc,
            register_edit_email, register_edit_name;
    private Spinner register_spinner_sex;
    private ArrayAdapter spinnerAdapter;
    private Button register_btn_cancel, register_btn_sign;

    private String id, pw, pwc, email, name, sex;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dialog_register);

        register_btn_cancel = (Button) findViewById(R.id.register_btn_cancel);
        register_btn_sign = (Button) findViewById(R.id.register_btn_sign);
        register_edit_id = (EditText) findViewById(R.id.register_edit_id);
        register_edit_pw = (EditText) findViewById(R.id.register_edit_pw);
        register_edit_pwc = (EditText) findViewById(R.id.register_edit_pwc);
        register_edit_email = (EditText) findViewById(R.id.register_edit_email);
        register_edit_name = (EditText) findViewById(R.id.register_edit_name);
        register_spinner_sex = (Spinner) findViewById(R.id.register_spinner_sex);

        register_btn_cancel.setOnClickListener(this);
        register_btn_sign.setOnClickListener(this);

        spinnerAdapter = ArrayAdapter.createFromResource(getApplicationContext(), R.array.register_spinner_sex, R.layout.spinner_sex_item);
        spinnerAdapter.setDropDownViewResource(R.layout.spinner_sex_drop);
        register_spinner_sex.setAdapter(spinnerAdapter);
        register_spinner_sex.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                sex = parent.getItemAtPosition(position).toString().trim();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                sex = parent.getItemAtPosition(0).toString().trim();
            }
        });

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.register_btn_cancel : onCancel(); break;
            case R.id.register_btn_sign : onSign(); break;
        }
    }

    private void onSign() {
        id = register_edit_id.getText().toString().trim();
        pw = register_edit_pw.getText().toString().trim();
        pwc = register_edit_pwc.getText().toString().trim();
        email = register_edit_email.getText().toString().trim();
        name = register_edit_name.getText().toString().trim();

        if(id.equals("")||pw.equals("")||pwc.equals("")||email.equals("")||name.equals("")||sex.equals("")){
            Toast.makeText(getApplicationContext(), "흠.. 뭔가 없는 것 같은데..", Toast.LENGTH_SHORT).show();
            return;
        }

        if(!pw.equals(pwc)){
            Toast.makeText(getApplicationContext(), "비밀번호 좀 제대로 입력해봐", Toast.LENGTH_SHORT).show();
            return;
        }

        SignTask signTask = new SignTask();
        signTask.execute();
    }

    private void signService(){
        Retrofit retrofit;
        HamonyService hamonyService;
        Call<UserData> signUp;

        retrofit = new Retrofit.Builder()
                .baseUrl("http://malang.moe:9000")
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        hamonyService = retrofit.create(HamonyService.class);
        signUp = hamonyService.sign(id, pw, email, name, sex);
        signUp.enqueue(new Callback<UserData>() {
            @Override
            public void onResponse(Response<UserData> response, Retrofit retrofit) {
                String id = response.body().get_id();
                int code = response.code();

                Toast.makeText(RegisterActivity.this, code + " : " + id, Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onFailure(Throwable t) {
                Toast.makeText(RegisterActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void onCancel() {
        finish();
    }

    class SignTask extends AsyncTask<Void, Void, Void>{
        private ProgressDialog dialog;


        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            dialog = ProgressDialog.show(RegisterActivity.this, "Sign Up", "Waiting for Server...");
        }

        @Override
        protected Void doInBackground(Void... params) {
            signService();

            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);

            dialog.dismiss();
        }
    }
}
