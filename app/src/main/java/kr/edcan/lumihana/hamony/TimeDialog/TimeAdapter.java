package kr.edcan.lumihana.hamony.TimeDialog;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;

import kr.edcan.lumihana.hamony.R;

/**
 * Created by kimok_000 on 2016-06-30.
 */
public class TimeAdapter extends ArrayAdapter<TimeData>{
    private LayoutInflater inflater;
    private Context context;

    public TimeAdapter(Context context,  ArrayList<TimeData> arrayList) {
        super(context, 0, arrayList);

        this.context = context;
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View v = null;

        if(convertView == null) v = inflater.inflate(R.layout.listview_content_time, null);
        else v = convertView;

        final TimeData data = this.getItem(position);

        if(data != null){
            TextView timeText = (TextView) v.findViewById(R.id.time_text);
            timeText.setText(data.getTimeText());
        }

        return v;
    }
}
