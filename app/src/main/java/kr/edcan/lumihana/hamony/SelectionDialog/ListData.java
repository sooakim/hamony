package kr.edcan.lumihana.hamony.SelectionDialog;

import kr.edcan.lumihana.hamony.ArrayData;

/**
 * Created by kimok_000 on 2016-05-11.
 */
public class ListData extends ArrayData{
    private String title;
    private String content;

    public ListData(String title, String content) {
        this.title = title;
        this.content = content;
    }

    public String getTitle() {
        return title;
    }

    public String getContent() {
        return content;
    }
}
