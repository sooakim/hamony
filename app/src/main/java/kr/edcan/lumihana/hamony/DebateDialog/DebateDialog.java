package kr.edcan.lumihana.hamony.DebateDialog;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import kr.edcan.lumihana.hamony.R;
import kr.edcan.lumihana.hamony.TimeDialog.TimeDialog;

/**
 * Created by kimok_000 on 2016-06-30.
 */
public abstract class DebateDialog extends Dialog implements View.OnClickListener {
    private EditText edit_title, edit_content;
    private TextView text_name, text_apply, text_cancel;
    private ImageView image_timer;

    private String selectedTime = "1시간 후";

    protected abstract void onApply(String title, String content, String name, String selectedTime);

    protected abstract void onCancel();

    public DebateDialog(Context context) {
        super(context);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        WindowManager.LayoutParams lpWindow = new WindowManager.LayoutParams();
        lpWindow.flags = WindowManager.LayoutParams.FLAG_DIM_BEHIND;
        lpWindow.dimAmount = 0.8f;
        getWindow().setAttributes(lpWindow);

        setContentView(R.layout.dialog_debate);

        edit_title = (EditText) findViewById(R.id.debate_edit_title);
        edit_content = (EditText) findViewById(R.id.debate_edit_content);
        text_name = (TextView) findViewById(R.id.debate_text_name);
        text_apply = (TextView) findViewById(R.id.dabate_text_apply);
        text_cancel = (TextView) findViewById(R.id.debate_text_cancel);
        image_timer = (ImageView) findViewById(R.id.debate_image_timer);

        text_apply.setOnClickListener(this);
        text_cancel.setOnClickListener(this);
        image_timer.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.dabate_text_apply: {
                String name = text_name.getText().toString().trim();
                String title = edit_title.getText().toString().trim();
                String content = edit_content.getText().toString().trim();

                if (name.equals("") || title.equals("") || content.equals("")) {
                    Toast.makeText(getContext(), "입력 내용을 확인해주세요", Toast.LENGTH_SHORT).show();
                    return;
                }

                onApply(title, content, name, selectedTime);
                break;
            }
            case R.id.debate_text_cancel:
                onCancel();
                break;
            case R.id.debate_image_timer: {
                TimeDialog dialog = new TimeDialog(getContext()) {
                    @Override
                    protected void onApply(String time) {
                        selectedTime = time;
                        dismiss();
                    }
                };

                dialog.show();

                break;
            }
        }
    }
}
