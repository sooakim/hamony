package kr.edcan.lumihana.hamony;

/**
 * Created by kimok_000 on 2016-07-02.
 */
public class UserData extends ArrayData{

    /**
     * id : heyou
     * pw : admin
     * email : kkkk
     * name : kk
     * sex : yx
     * _id : 5776a974c907c6a51a197a73
     * __v : 0
     * joinopinion : null
     */

    private String id;
    private String pw;
    private String email;
    private String name;
    private String sex;
    private String _id;
    private int __v;
    private String joinopinion;

    public UserData(String id, String pw, String email, String name, String sex, String _id, int __v, String joinopinion) {
        this.id = id;
        this.pw = pw;
        this.email = email;
        this.name = name;
        this.sex = sex;
        this._id = _id;
        this.__v = __v;
        this.joinopinion = joinopinion;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getPw() {
        return pw;
    }

    public void setPw(String pw) {
        this.pw = pw;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    public int get__v() {
        return __v;
    }

    public void set__v(int __v) {
        this.__v = __v;
    }

    public String getJoinopinion() {
        return joinopinion;
    }

    public void setJoinopinion(String joinopinion) {
        this.joinopinion = joinopinion;
    }
}
