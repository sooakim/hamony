package kr.edcan.lumihana.hamony.GBCommentActivity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;

import kr.edcan.lumihana.hamony.ArrayData;
import kr.edcan.lumihana.hamony.ListViewTool;
import kr.edcan.lumihana.hamony.R;

public class CommentActivity extends AppCompatActivity implements View.OnClickListener, ListViewTool {
    private ListView listView;
    private CommentAdapter adapter;
    private ArrayList<CommentData> arrayList;
    private TextView text_send, text_goodNumber, text_badNumber;
    private EditText edit_comment;
    private ImageView image_good, image_bad;

    private int good, bad;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gbcomment);

        listView = (ListView) findViewById(R.id.comment_list_comments);
        text_send = (TextView) findViewById(R.id.comment_text_send);
        text_goodNumber = (TextView) findViewById(R.id.comment_text_goodNumber);
        text_badNumber = (TextView) findViewById(R.id.comment_text_badNumber);
        edit_comment = (EditText) findViewById(R.id.comment_edit_comment);
        image_good = (ImageView) findViewById(R.id.comment_image_good);
        image_bad = (ImageView) findViewById(R.id.comment_image_bad);

        text_send.setOnClickListener(this);
        image_good.setOnClickListener(this);
        image_bad.setOnClickListener(this);

        initListView();
        addListView(new CommentData("오준석", "2048", "니코니코니", true));
    }

    @Override
    public void initListView() {
        arrayList = new ArrayList<>();
        setListView();
    }

    @Override
    public void addListView(ArrayData data) {
        arrayList.add((CommentData) data);
        setListView();
    }

    @Override
    public void setListView() {
        adapter = new CommentAdapter(getApplicationContext(), arrayList);
        listView.setAdapter(adapter);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.comment_text_send:
                onSend();
                break;
            case R.id.comment_image_good:
                onGood();
                break;
            case R.id.comment_image_bad:
                onBad();
                break;
        }
    }

    private void onBad() {
        text_badNumber.setText(++bad + "");
    }

    private void onGood() {
        text_goodNumber.setText(++good + "");
    }

    private void onSend() {
        String content = edit_comment.getText().toString().trim();

        if (content == null) return;
        CommentData data = new CommentData("익명", "80", content, false);

        addListView(data);
    }
}
