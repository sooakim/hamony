package kr.edcan.lumihana.hamony.GBCommentActivity;

import kr.edcan.lumihana.hamony.ArrayData;

/**
 * Created by kimok_000 on 2016-06-22.
 */
public class CommentData extends ArrayData{
    private String nickName;
    private String likes;
    private String content;
    private boolean isTheBest = false;

    public CommentData(String nickName, String likes, String content, boolean isTheBest) {
        this.nickName = nickName;
        this.likes = likes;
        this.content = content;
        this.isTheBest = isTheBest;
    }

    public String getNickName() {
        return nickName;
    }

    public String getLikes() {
        return likes;
    }

    public String getContent() {
        return content;
    }

    public boolean getIsTheBest(){
        return isTheBest;
    }
}
