package kr.edcan.lumihana.hamony.MainActivity;

import kr.edcan.lumihana.hamony.ArrayData;

/**
 * Created by kimok_000 on 2016-05-09.
 */
public class RecyclerData extends ArrayData{
    public final static int DATA_STATUS_NONE = 0;
    public final static int DATA_STATUS_DEBATE = 1;
    public final static int DATA_STATUS_SYMPATHY = 2;
    public final static int DATA_STATUS_VOTE = 3;
    public final static int DATA_STATUS_COMMENT = 4;

    public final static int UPLOAD_TIME_JUST_NOW = 0;

    private String content;
    private String userName;
    private int currentStatus;
    private int uploadTime;
    private int commentNum;

    public RecyclerData(String content, String userName, int currentStatus, int uploadTime, int commentNum) {
        if (content != null) this.content = content;
        else this.content = "표시할 내용 없음";

        if (userName != null) this.userName = userName;
        else this.userName = "익명";

        if (currentStatus != DATA_STATUS_NONE) this.currentStatus = currentStatus;
        else this.currentStatus = DATA_STATUS_NONE;

        if (uploadTime != UPLOAD_TIME_JUST_NOW) this.uploadTime = uploadTime;
        else this.uploadTime = UPLOAD_TIME_JUST_NOW;

        if (commentNum != 0) this.commentNum = commentNum;
        else this.commentNum = 0;
    }

    public String getContent() {
        return content;
    }

    public String getUserName() {
        return userName;
    }

    public int getCurrentStatus() {
        return currentStatus;
    }

    public int getUploadTime() {
        return uploadTime;
    }

    public int getCommentNum() {
        return commentNum;
    }
}
