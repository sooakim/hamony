package kr.edcan.lumihana.hamony.GBCommentActivity;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;

import kr.edcan.lumihana.hamony.R;

/**
 * Created by kimok_000 on 2016-06-22.
 */
public class CommentAdapter extends ArrayAdapter<CommentData> {
    private LayoutInflater inflater;
    private Context context;

    public CommentAdapter(Context context, ArrayList<CommentData> arrayList) {
        super(context, 0, arrayList);
        this.context = context;

        if (inflater == null)
            inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View v = null;

        if (convertView == null) v = inflater.inflate(R.layout.listview_content_comment, null);
        else v = convertView;

        final CommentData data = this.getItem(position);

        if (data != null) {
            TextView nickName, likes;
            LinearLayout normal = (LinearLayout) v.findViewById(R.id.comment_Linear_normal);
            LinearLayout best = (LinearLayout) v.findViewById(R.id.comment_Linear_best);

            if (data.getIsTheBest()) {
                nickName = (TextView) v.findViewById(R.id.comment_best_text_name);
                likes = (TextView) v.findViewById(R.id.comment_best_text_likes);
                if (!data.getLikes().equals("")) likes.setText("공감 수 : " + data.getLikes());
                else likes.setText("공감 없음");

                best.setVisibility(View.VISIBLE);
                normal.setVisibility(View.GONE);
            } else {
                nickName = (TextView) v.findViewById(R.id.comment_text_normal_name);
            }

            TextView content = (TextView) v.findViewById(R.id.comment_text_content);

            if (!data.getNickName().equals(""))
                nickName.setText(data.getNickName().toString().trim());
            else nickName.setText("닉네임 없음");


            if (!data.getContent().equals(""))
                content.setText(data.getContent().toString().trim());
            else content.setText("내용 없음");
        }

        return v;
    }
}
