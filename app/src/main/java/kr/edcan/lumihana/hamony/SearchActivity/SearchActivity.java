package kr.edcan.lumihana.hamony.SearchActivity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;

import kr.edcan.lumihana.hamony.R;

public class SearchActivity extends AppCompatActivity implements View.OnClickListener{
    private ImageView image_back;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);

        image_back = (ImageView) findViewById(R.id.search_image_back);
        image_back.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.search_image_back :
                onBack();
                break;
        }
    }

    private void onBack() {
        finish();
    }
}
